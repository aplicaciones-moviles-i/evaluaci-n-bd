package facci.pm.evaluacionsqlite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

public class UniversidadAdapter extends ArrayAdapter<Universidad> {
    private Context context;
    private List<Universidad> ListaUniversidades;

    public UniversidadAdapter(Context context, List<Universidad> listaUniversidades) {
        super(context, R.layout.list_item_primera_tabla);
        this.context = context;
        this.ListaUniversidades = listaUniversidades;
    }

    @Override
    public int getCount() {
        return ListaUniversidades.size();
    }

    @Override
    public Universidad getItem(int position) {
        return  ListaUniversidades.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ListaUniversidades.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder viewHolder;

        if (convertView == null || convertView.getTag() == null) {
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.list_item_primera_tabla, parent, false);
            viewHolder.TextViewNombre = view.findViewById(R.id.TextViewNombre);
            viewHolder.TextViewCategoria = view.findViewById(R.id.TextViewCategoria);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            view = convertView;
        }

        // Set text with the item name
        //viewHolder.TextViewNombre = view.findViewById(R.id.TextViewNombre);
        viewHolder.TextViewNombre.setText(ListaUniversidades.get(position).getNombre());
        viewHolder.TextViewCategoria.setText(ListaUniversidades.get(position).getCategoria());

        return view;
    }

    static class ViewHolder {
        TextView TextViewNombre;
        TextView TextViewCategoria;
    }
}
