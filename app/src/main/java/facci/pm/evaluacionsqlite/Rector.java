package facci.pm.evaluacionsqlite;

import com.orm.SugarRecord;

public class Rector extends SugarRecord<Rector> {

    private String Nombres;
    private String Apellidos;
    private String Titulo;
    private String IDUniversidad;


    public Rector(String Nombres, String apellidos, String titulo, String IDUniversidad) {
        this.Nombres = Nombres;
        this.Apellidos = apellidos;
        this.Titulo = titulo;
        this.IDUniversidad = IDUniversidad;
    }

    public Rector() {
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getIDUniversidad() {
        return IDUniversidad;
    }

    public void setIDUniversidad(String IDUniversidad) {
        this.IDUniversidad = IDUniversidad;
    }
}
