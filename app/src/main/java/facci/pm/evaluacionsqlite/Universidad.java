package facci.pm.evaluacionsqlite;

import com.orm.SugarRecord;

public class Universidad extends SugarRecord<Universidad>  {


    //CAMPOS
    String Siglas;
    String Nombre;
    String Categoria;
    String Ciudad;

    //CONSTRUCTOR
    public Universidad(String siglas, String nombre, String categoria, String ciudad) {
        this.Siglas = siglas;
        Nombre = nombre;
        Categoria = categoria;
        Ciudad = ciudad;
    }

    //SOBRECARGA DEL CONSTRUCTOR
    public Universidad() {
    }

    //ENCAPSULACIÓN


    public String getSiglas() {
        return Siglas;
    }

    public void setSiglas(String siglas) {
        Siglas = siglas;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        Categoria = categoria;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setCiudad(String ciudad) {
        Ciudad = ciudad;
    }
}
