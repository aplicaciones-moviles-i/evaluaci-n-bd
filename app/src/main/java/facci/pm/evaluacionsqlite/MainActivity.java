package facci.pm.evaluacionsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements  ListView.OnItemClickListener {

    private ListView ListViewUniversidades;
    private UniversidadAdapter universidadAdapter;
    private List<Universidad> listaUniversidades = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListViewUniversidades = findViewById(R.id.ListViewUniversidades);
        //Click en el item
        ListViewUniversidades.setOnItemClickListener(this);

        //Ingreso de datos estáticos
        Universidad universidad = new Universidad(
                "ESPAM", "Escuela Superior",
                "Acreditada", "Calceta" );
        universidad.save();


        LlenarListaUniversidades();


        universidadAdapter = new UniversidadAdapter(this, listaUniversidades);
        ListViewUniversidades.setAdapter(universidadAdapter);


    }

    public void LlenarListaUniversidades(){

        listaUniversidades = Universidad.listAll(Universidad.class);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        Toast.makeText(this, String.valueOf(
                listaUniversidades.get(position).getId()), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(
                MainActivity.this, RectorActivity.class);
        intent.putExtra("id", listaUniversidades.get(position).getId().toString());
        startActivity(intent);
    }
}
